[Blake_Stone]: screenshots/blake_stone.png
[Blake_Stone_Alt]: screenshots/blake_stone_alt.png
[Doom_City]: screenshots/doom_city.png
[Doom_Tech]: screenshots/doom_tech.png
[Doom_Hell]: screenshots/doom_hell.png
[Eradicator]: screenshots/eradicator.png
[Hexen_2]: screenshots/hexen2.png
[IPOG]: screenshots/in_pursuit_of_greed.png
[Lode_Runner]: screenshots/lode_runner.png
[Lode_Runner_2]: screenshots/lode_runner_2.png
[Lost_Garden_Blue]: screenshots/lost_garden_blue.png
[Lost_Garden_Cyan]: screenshots/lost_garden_cyan.png
[Lost_Garden_Green]: screenshots/lost_garden_green.png
[Lost_Garden_Purple]: screenshots/lost_garden_purple.png
[Lost_Garden_Red]: screenshots/lost_garden_red.png
[Lost_Garden_White]: screenshots/lost_garden_white.png
[Lost_Garden_Yellow]: screenshots/lost_garden_yellow.png
[Necrodome]: screenshots/necrodome.png
[POD]: screenshots/planet_of_death.png
[Quake1]: screenshots/quake1.png
[Quake1_Headers]: screenshots/quake1_headers.png
[Quake2]: screenshots/quake2.png
[Quake3]: screenshots/quake3.png
[Rise_of_the_Triad]: screenshots/rise_of_the_triad.png
[Simple]: screenshots/simple.png
[Tyrian]: screenshots/tyrian.png
[WarWind_Archaeological]: screenshots/warwind_archaeological.png
[WarWind_Eaggra]: screenshots/warwind_eaggra.png
[WarWind_Obblinox]: screenshots/warwind_obblinox.png
[WarWind_ShamaLi]: screenshots/warwind_shamali.png
[WarWind_ThaRoon]: screenshots/warwind_tharoon.png


# Site Themes

This is the repo for website themes I'm making. Each theme has test demo site implemented and extra resources. Feel free to use them and to contribute by any means!


## Licensing

Site Themes project is licensed under the [GPL-3](http://www.gnu.org/licenses/gpl.html).

The data files (artwork, svg files, etc) are not covered in this license. The original sources are listed below.


## Themes

The themes are as follows (including credits for base graphics):

**Blake Stone:** JAM Productions (from [Blake Stone](https://blakestone.fandom.com/wiki/Blake_Stone_Wiki))

![Blake_Stone]

![Blake_Stone_Alt]


**Doom:** id Software (from [Doom](https://en.wikipedia.org/wiki/Classic_Doom))

**Doom - City**

![Doom_City]

**Doom - Hell**

![Doom_Hell]

**Doom - Tech**

![Doom_Tech]


**Eradicator:** Accolade (from [Eradicator](https://en.wikipedia.org/wiki/Eradicator_(video_game)))

![Eradicator]


**Hexen 2:** Raven Software (from [Hexen 2](https://en.wikipedia.org/wiki/Hexen_II))

![Hexen_2]


**In Pursuit of Greed:** Mind Shear Software (from [In Pursuit of Greed](https://archive.org/details/GreedSource))

![IPOG]


**Lode Runner Online: The Mad Monks' Revenge:** Presage Software (from [Lode Runner Online](https://en.wikipedia.org/wiki/Lode_Runner_Online:_The_Mad_Monks%27_Revenge))

![Lode_Runner]


**Lode Runner 2:** Presage Software (from [Lode Runner 2](https://www.wikipedia.org/wiki/Lode_Runner_2))

![Lode_Runner_2]


**Lost Garden:** Daniel Cook (from [Tyrian](https://www.spriters-resource.com/pc_computer/tyrian/), [Hard Vacuum](https://lostgarden.home.blog/2005/03/27/game-post-mortem-hard-vacuum/), and
[Iron Plague](https://lostgarden.home.blog/2005/03/30/download-a-complete-set-of-sweet-8-bit-sinistar-clone-graphics/))

**Lost Garden - Blue**

![Lost_Garden_Blue]

**Lost Garden - Cyan**

![Lost_Garden_Cyan]

**Lost Garden - Green**

![Lost_Garden_Green]

**Lost Garden - Purple**

![Lost_Garden_Purple]

**Lost Garden - Red**

![Lost_Garden_Red]

**Lost Garden - White**

![Lost_Garden_White]

**Lost Garden - Yellow**

![Lost_Garden_Yellow]


**Necrodome:** Raven Software (from [Necrodome](https://en.wikipedia.org/wiki/Necrodome))

![Necrodome]


**Planet of Death:** Ubisoft (from [Planet of Death Fandom](https://en.wikipedia.org/wiki/POD_(video_game)))

![POD]


**Quake 1:** id Software, Hipnotic Interactive and Rogue Entertainment (from [Quake 1](https://en.wikipedia.org/wiki/Quake_(video_game))).

![Quake1]

![Quake1_Headers]


**Quake 2:** id Software (from [Quake 2](https://en.wikipedia.org/wiki/Quake_II))

![Quake2]


**Quake 3:** id Software (from [Quake 3: Arena](https://en.wikipedia.org/wiki/Quake_III_Arena) and [High Quality Quake](https://www.moddb.com/mods/high-quality-quake))

![Quake3]


**Rise of the Triad:** Apogee Software (from [Rise of the Triad](https://www.wikipedia.org/wiki/Rise_of_the_Triad))

![Rise_of_the_Triad]


**Simple:**

![Simple]


**Tyrian:** Daniel Cook (from [Tyrian](https://www.spriters-resource.com/pc_computer/tyrian/))

![Tyrian]


**War Wind:** Dreamforge Intertainment (from [War Wind](https://wikipedia.org/wiki/War_Wind))

**War Wind - Archaeological**

![WarWind_Archaeological]

**War Wind - Eaggra**

![WarWind_Eaggra]

**War Wind - Obblinox**

![WarWind_Obblinox]

**War Wind - Shama'Li**

![WarWind_ShamaLi]

**War Wind - Tha'Roon**

![WarWind_ThaRoon]
